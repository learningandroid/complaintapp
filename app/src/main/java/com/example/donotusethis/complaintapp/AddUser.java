package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class AddUser extends AppCompatActivity {


     Spinner UserType;
     Spinner Hostel;
     EditText RoomNo;
     EditText WorkType;
     int User=1;

    AddUser adduser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        adduser=this;

        Hostel = (Spinner)findViewById(R.id.hostel);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(adduser, android.R.layout.simple_spinner_dropdown_item, constants.hostels);
        Hostel.setAdapter(adapter1);

        RoomNo=(EditText)findViewById(R.id.RoomNo);
        WorkType=(EditText)findViewById(R.id.WorkType);

        UserType = (Spinner)findViewById(R.id.UserType);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(adduser, android.R.layout.simple_spinner_dropdown_item, constants.UserTypes);
        UserType.setAdapter(adapter);

        UserType.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        User=position+1;
                        switch (position) {
                            case 0:
                                Hostel.setVisibility(View.VISIBLE);
                                RoomNo.setVisibility(View.VISIBLE);
                                WorkType.setVisibility(View.GONE);
                                break;
                            case 1:
                                Hostel.setVisibility(View.GONE);
                                RoomNo.setVisibility(View.GONE);
                                WorkType.setVisibility(View.GONE);
                                break;
                            case 2:
                                Hostel.setVisibility(View.VISIBLE);
                                RoomNo.setVisibility(View.GONE);
                                WorkType.setVisibility(View.GONE);
                                break;
                            case 3:
                                Hostel.setVisibility(View.GONE);
                                RoomNo.setVisibility(View.GONE);
                                WorkType.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                             //do nothing
                    }
                }
        );


        Button Add=(Button)findViewById(R.id.adduser);

        Add.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       AddUser();
                                   }
                               }

        );

    }

    public void AddUser(){

        String UserId=((EditText)findViewById(R.id.UserId)).getText().toString();
        String Password=((EditText)findViewById(R.id.Password)).getText().toString();
        final String Firstname=((EditText)findViewById(R.id.firstname)).getText().toString();
        final String Lastname=((EditText)findViewById(R.id.lastname)).getText().toString();
        String RoomN=RoomNo.getText().toString();
        String HostelN=Hostel.getSelectedItem().toString();
        String WorkT=WorkType.getText().toString();

        String url=LoginActivity.base_url+"users/add?UserId="+UserId+"&Password="+Password+"&FirstName="+Firstname+"&LastName="
                +Lastname+"&HostelName="+HostelN+"&type="+User+"&Type="+WorkT+"&RoomNo="+RoomN;
        RequestQueue requestQueue= Volley.newRequestQueue(adduser);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                Toast.makeText(adduser, "Succesfully added "+Firstname+" "+Lastname, Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(adduser, AddUser.class));
                            }else{
                                Toast.makeText(adduser, "Not possible", Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(adduser, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message=error.getMessage();
                        if (message==null)
                            Toast.makeText(adduser,
                                    "Message", Toast.LENGTH_SHORT).show();
                        else
                        if (message.contains("java.net.Connect"))
                            Toast.makeText(adduser,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(adduser, message, Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }
}


