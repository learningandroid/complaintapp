package com.example.donotusethis.complaintapp;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

public class AllComplaints extends AppCompatActivity {


    String individual_response, hostel_response, institute_response;
    ViewPager viewPager;
    TabLayout tabLayout;
    int user_type;
    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_complaints);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            user_type = bundle.getInt("type");
            user_id = bundle.getString("user_id");
        }

        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        loadAll();
    }

    public void viewCreate(){
        ViewPagerAdapter adapter= new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new IndividualComplaints(), "INDIVIDUAL");
        viewPager.setAdapter(adapter);
        if (user_type == constants.cons_student || user_type == constants.cons_warden){
            adapter.addFragment(new HostelComplaints(), "HOSTEL");
            viewPager.setAdapter(adapter);
        }
        adapter.addFragment(new InstituteComplaints(), "INSTITUTE");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void loadAll(){

        final AllComplaints allComplaints=this;
        final String url = LoginActivity.base_url+"complaints/";
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        final CustomRequest individual_complaints=new CustomRequest(
                Request.Method.GET, url+"/individual",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        individual_response=response;
                        viewCreate();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(allComplaints, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        final CustomRequest hostel_complaints=new CustomRequest(
                Request.Method.GET, url+"/hostel",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hostel_response=response;
                        viewCreate();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(allComplaints, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        CustomRequest institute_complaints=new CustomRequest(
                Request.Method.GET, url+"/institute",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        institute_response=response;
                        viewCreate();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(allComplaints, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(individual_complaints);
        requestQueue.add(hostel_complaints);
        requestQueue.add(institute_complaints);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
