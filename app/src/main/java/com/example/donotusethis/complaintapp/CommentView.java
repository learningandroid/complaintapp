package com.example.donotusethis.complaintapp;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HARISH on 28-03-2016.
 */
public class CommentView extends ArrayAdapter<JSONObject> {

    public CommentView(Context context, JSONObject[] objects) {
        super(context, R.layout.frg_comment_view, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater myInflater= LayoutInflater.from(getContext());
        View myCustomview= myInflater.inflate(R.layout.frg_comment_view, parent, false);

        try{
            JSONObject singleItem= getItem(position);


            TextView comment_text = (TextView) myCustomview.findViewById(R.id.comment);
                    comment_text.setText(
                            singleItem.getString("Content")
                    );
            TextView time_text = (TextView)myCustomview.findViewById(R.id.time);
            time_text.setText(
                    singleItem.getString("CreatedTime")
            );
            if (singleItem.getBoolean("right")){
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT
                );
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                myCustomview.findViewById(R.id.shift).setLayoutParams(layoutParams);
            }
        }catch (JSONException e){
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return myCustomview;
    }
}
