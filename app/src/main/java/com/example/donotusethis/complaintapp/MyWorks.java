package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyWorks extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_works);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadAll();
    }

    public void loadAll(){
        String url=LoginActivity.base_url+"complaints/work";
        final MyWorks description = this;
        RequestQueue requestQueue= Volley.newRequestQueue(description);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                JSONArray pinned = jsonObject.getJSONArray("pinned");
                                JSONArray unpinned = jsonObject.getJSONArray("unpinned");
                                int pin_len = pinned.length();
                                int unpin_len = unpinned.length();
                                final JSONObject[] complaints=new JSONObject[pin_len+unpin_len];
                                for (int i=0;i<pin_len;i++) {
                                    complaints[i] = pinned.getJSONObject(i);
                                    complaints[i].put("pin", constants.pinned);
                                }
                                for (int i=0; i<unpin_len; i++) {
                                    complaints[pin_len + i] = unpinned.getJSONObject(i);
                                    complaints[pin_len+i].put("pin", constants.unpinned);
                                }
                                ListView listView = (ListView)findViewById(R.id.works_list);
                                ListAdapter listAdapter = new WorksAdapter(description, complaints);
                                listView.setAdapter(listAdapter);
                                listView.setOnItemClickListener(
                                        new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                Intent intent = new Intent(description, IndividualDescription.class);
                                                try {
                                                    intent.putExtra("id", complaints[position].getString("id"));
                                                    intent.putExtra("sender", constants.receiver);
                                                } catch (JSONException e) {
                                                    Toast.makeText(description, e.getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                                startActivity(intent);
                                            }
                                        }
                                );
                            }else{
                                Toast.makeText(description, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(description, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(description, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },description
        );
        requestQueue.add(stringRequest);
    }
}
